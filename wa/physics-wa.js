export async function initPhysics() {
  // Bitbucket quirk:
  // instantiateStreaming requires response to have application/wasm MIME type
  // BitBucket returns application/octet-stream
  const res = await WebAssembly.instantiate(
		await (await fetch('physics.wasm')).arrayBuffer(),
		{ Math }
	);

	const wasmExports = res.instance.exports;

  let f32a = new Float32Array(0);

  function getData() {
    if (wasmExports.mem.buffer !== f32a.buffer) {
      f32a = new Float32Array(wasmExports.mem.buffer);
    }
    return f32a;
  }

  return {
    getData,
    tick: wasmExports.tick,
    fire: wasmExports.fire,
  }
}
